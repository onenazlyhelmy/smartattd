<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employeelog extends Model
{
    //
    protected $table = "rawlog";

    protected $guarded = ['id'];
}

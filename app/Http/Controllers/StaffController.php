<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;
use App\Employee;


class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $employees = Employee::all();
      return view('staff.staff-list',compact('employees'));
      //  return view('staff.staff-list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

//      \Debugbar::info('return view staff.staff-create');
       return view('staff.staff-create');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {

      \Debugbar::info('return view staff.staff-create');
        return view('staff.staff-create');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function searchandshow($id)
    {



    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    //    echo("<script>alert('huuh!');</script>");
    // $debugbar = new StandardDebugBar();
    // $debugbarRenderer = $debugbar->getJavascriptRenderer();
      $employee = Employee::create($request->all());
            // \Debugbar::info(print_r($request->all()));
       \Debugbar::info($request->all());
    //   echo "<script>alert('".print_r($request->all())."');</script>";
       // $employee = new Employee();
       //  $employee->name =  $request->get('name');
       //  $employee->mfgno = $request->get('mfgno');
       //  $employee->ic_number = $request->get('ic_number');
       //    $employee->position = $request->get('position');
       //    $employee->department = $request->get('department');
       //            $employee->address = $request->get('address');
       //           $employee->save();

     // $str = bin2hex($request['mfgno']);
    //    $nm = $request['name'];
    //    \Debugbar::info("ID: " .$nm);


  //      Redis::publish('mfgno',$employee->mfgno);
        //
      	// Redis::lpush('mfgnohex',$employee->mfgno);
         $name = $employee->name;
         $ids = $employee->id;

       	Redis::hset('mfg',$employee->mfgno,'{ "name" : " '.  $name.' ","staffid":"'.  $ids.'","priv":"0#1#0#0#1#0","privdays":"1#1#1#1#1#1#1"}');

       // if(!$employee->id){
       //
       //     return response()->json(['error' => 'Employee Fail To Create'], 404);
       //
       // }
      //     $employees = Employee::all();
          //return redirect('staff.staff-list')->with('success','Employee has been added');
         // return response()->json(['success' => 'Employee Successfully Add'], 201);
         $employees = Employee::all();
         return view('staff.staff-list',compact('employees'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('attendance.personal-attendance');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $employee = Employee::find($id);
          return view('staff.staff-edit',compact('employee','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->name = $request->get('name');
        $name = $request->get('name');
        $employee->mfgno = $request->get('mfgno');
        $employee->email = $request->get('email');
        $employee->department = $request->get('department');
        $employee->position = $request->get('position');
        $employee->address=$request->get('address');
        $employee->save();
        Redis::hset('mfg',$employee->mfgno,'{ "name" : " '.  $name .' ","staffid":"'.  $id.'","priv":"0#1#0#0#1#0","privdays":"1#1#1#1#1#1#1"}');
        $employees = Employee::all();
        return view('staff.staff-list',compact('employees'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
   {
       $employee = Employee::find($id);
       Redis::hdel('mfg',$employee->mfgno);
       $employee->delete();
         return redirect('staff')->with('success','Staff Has Been Deleted');
   }
   public function logout()
    {
        auth()->logout();
        return redirect('/home');
    }
}

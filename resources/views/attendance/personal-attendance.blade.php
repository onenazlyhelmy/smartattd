@extends('layout.main-app')
@section('content')
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
  <div class="row">
     <div class="col-lg-3 grid-margin stretch-card"></div>
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Staff One</h4>
          <p class="card-description">
            Staff Daily Attendance Record
          </p>
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>
                   Attendance Record For Today
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    7.30 AM [First Check In]
                  </td>
                </tr>
                <tr>
                  <td>
                    8.30 PM [Breakfast Outing]
                  </td>
                </tr>
                <tr>
                  <td>
                    9.00 AM [Check In After Breakfast]
                  </td>
                </tr>
                <tr>
                  <td>
                    12.30 PM [Check Out For Lunch]
                  </td>
                </tr>
                <tr>
                  <td>
                    12.30 PM [Check Out For Lunch]
                  </td>
                </tr>
                <tr>
                  <td>
                    1.30 PM [Back From Lunch]
                  </td>
                </tr>
                <tr>
                  <td>
                    5.00 PM [End Of Work]
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br>
              <center><label class="badge badge-success">Daily Attendance Complete</label></center>
        </div>
      </div>
    </div>

     <div class="col-lg-3 grid-margin stretch-card"></div>
  </div>
</div>
<!-- content-wrapper ends -->
@stop
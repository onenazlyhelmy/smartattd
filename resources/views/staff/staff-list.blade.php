@extends('layout.main-app')
@section('content')
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">List Of Staff</h4>
          <p class="card-description">
            Admin Can View Details On Staff Activity
          </p>
          <div class="table-responsive">
           <table class="table table-bordered" id="datatab-userslst">
              <thead>
                <tr>
                  <th>
                  Staff ID
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                   	Card No
                  </th>
                  <th>
                   Delete
                  </th>
                  <th>
                    <center>Action</center>
                  </th>
                  <th>
                    <center>View Record</center>
                  </th>

                </tr>
              </thead>
              <tbody>

            @foreach($employees as $employee)
                <tr>

                  <td>{{$employee['id']}}</td>
                  <td>{{$employee['name']}}</td>
                  <td>
                   <!-- <button onclick="window.location.href='{{url('staff/create')}}'" type="button" class="btn btn-inverse-success btn-fw">View Details</button> -->
                   {{$employee['mfgno']}}
                  </td>
                  <td>
                    <form action="{{action('StaffController@destroy', $employee['id'])}}" method="post">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                  </form>
                  </td>
                  <td>
                   <center><a href="{{action('StaffController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a></center>
                  </td>
                  <td>
                   <center><a href="{{action('TimeSheetsController@show', $employee['id'])}}" class="btn btn-warning">View record</a></center>
                  </td>

                </tr>
           @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
      $('#datatab-userslst').DataTable({
          responsive: true
      });
  });


  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("active");
  });
  </script>
<!-- content-wrapper ends -->
@stop

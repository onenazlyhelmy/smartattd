@extends('layout.main-app')
@section('content')
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Update Staff</h4>
          <p class="card-description">
            Staff Detail
          </p>


            <form method="post" action="{{action('StaffController@update', $id)}}">
	           @csrf
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input name="_method" type="hidden" value="PATCH">
            <div class="row">
              <div class="col-md-12"></div>
              <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$employee->name}}">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"></div>
              <div class="form-group col-md-4">
                <label for="name">Card No:</label>
                <input type="text" class="form-control" id=mfgno name="mfgno" value="{{$employee->mfgno}}">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"></div>
              <div class="form-group col-md-4">
                <label for="name">Position:</label>
                <input type="text" class="form-control" name="position" value="{{$employee->position}}">
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Email address</label>
              <input name="email" type="email" class="form-control" id="email" value="{{$employee->email}}">
            </div>

            <div class="form-group">
              <label for="exampleInputCity1">Department</label>
              <input name="department" type="text" class="form-control" id="department" value="{{$employee->department}}">
            </div>
            <div class="form-group">
              <label for="exampleTextarea1">Address</label>
              <textarea name="address" class="form-control" id="address" rows="3"  >{{$employee->address}}</textarea>
            </div>

            <div class="row">
              <div class="col-md-12"></div>
              <div class="form-group col-md-12" style="margin-top:10px">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<script>
$("#addStaff").submit(function(e){
    e.preventDefault();
    $.ajax({
       type:'POST',
       url:'{{url('/staff')}}',
	   method: 'POST',
	   data: new FormData(this),
	   contentType: false,
	   cache: false,
	   processData: false,
       success:function(data){
          alert(data.success);
          $("#addStaff")[0].reset();
       }
    });
});
</script>

@stop

@extends('layout.main-app')
@section('content')
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">New Staff</h4>
          <p class="card-description">
            Register New Staff
          </p>
          <form id="addStaff" method="post" action="{{url('/staff')}}" enctype="multipart/form-data">
          	@csrf
            <div class="form-group">
              <label for="exampleInputName1">Name</label>
              <input name="name" type="text" class="form-control" id="name" placeholder="Name">
            </div>
            <div class="form-group">
              <label for="exampleInputCity1">Card Number</label>
              <input name="mfgno" type="text" class="form-control" id="mfgno" placeholder="Card Serial number">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail3">Email address</label>
              <input name="email" type="email" class="form-control" id="email" placeholder="Email">
            </div>
            <div class="form-group">
              <label for="exampleInputCity1">Position</label>
              <input name="position" type="text" class="form-control" id="position" placeholder="Position">
            </div>
            <div class="form-group">
              <label for="exampleInputCity1">Department</label>
              <input name="department" type="text" class="form-control" id="department" placeholder="Department">
            </div>
            <div class="form-group">
              <label for="exampleTextarea1">Address</label>
              <textarea name="address" class="form-control" id="address" rows="2"></textarea>
            </div>
            <button type="submit" class="btn btn-success mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<script>
// $("#addStaff").submit(function(e){
//     e.preventDefault();
//     $.ajax({
//        type:'POST',
//        url:'{{url('/staff')}}',
// 	   method: 'POST',
// 	   data: new FormData(this),
// 	   contentType: false,
// 	   cache: false,
// 	   processData: false,
//        success:function(data){
//         //  alert(data.success);
//           $("#addStaff")[0].reset();
//        }
//     });
// });
</script>

@stop

@extends('layout.main-app')
@section('content')
  <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">User Attendance Log List</h4>
          <p class="card-description">

            Daily Attendance

          </p>
          <div class="table-responsive">
            <table class="table table-bordered" id="datatab-searchuser">
              <thead>
                <tr>
                  <th>
                    DateTime
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                    Staff ID
                  </th>
                  <th>
                  LocationNo
                  </th>
                  <th>
                    <center>TerminalNo</center>
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach($logs as $log)
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <tr>
                  <td>
                   <label class="badge badge-success">{{$log->rawlog_dt}}</label>
                  </td>
                  <td>
                  {{$log->rawlog_name}}
                  </td>
                  <td>
                   {{$log->rawlog_staffid}}
                  </td>
                  <td>
                    {{$log->rawlog_locationno}}
                  </td>
                  <td>
                  {{$log->rawlog_terminalno}}
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
      $('#datatab-searchuser').DataTable({
          responsive: true
      });
  });


  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("active");
  });
  </script>
<!-- content-wrapper ends -->
@stop

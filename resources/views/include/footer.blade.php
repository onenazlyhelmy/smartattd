      <!-- partial:partials/_footer.html -->
      <footer class="footer">
        <div class="container-fluid clearfix">
          <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">#####

          <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">***********
            <i class="mdi mdi-heart text-danger"></i>
          </span>
        </div>
      </footer>
      <!-- partial -->
    </div>
    <!-- main-panel ends -->
  </div>
  <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{url('theme/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{url('theme/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{url('theme/js/off-canvas.js')}}"></script>
<script src="{{url('theme/js/misc.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{url('theme/js/dashboard.js')}}"></script>
<!-- End custom js for this page-->
  <!-- inject:js -->
  <script src="{{url('theme/js/chart.js')}}"></script>
  <!-- End custom js for this page-->
</body>
</html>

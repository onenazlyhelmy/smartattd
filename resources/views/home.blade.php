@extends('layout.main-app')
@section('content')
<div class="main-panel">
<div class="content-wrapper">
  <div class="row purchace-popup">
  </div>
	  <div class="row">
	    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
	      <div class="card card-statistics">
	        <div class="card-body">
	          <div class="clearfix">
	            <div class="float-left">
	              <i class="mdi mdi-cube text-danger icon-lg"></i>
	            </div>
	            <div class="float-right">
	              <p class="mb-0 text-right">Present Todays</p>
	              <div class="fluid-container">
	                <h3 class="font-weight-medium text-right mb-0">500</h3>
	              </div>
	            </div>
	          </div>
	          <p class="text-muted mt-3 mb-0">
	            {{-- <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> --}}Present Employee To Work For Today
	          </p>
	        </div>
	      </div>
	    </div>
	    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
	      <div class="card card-statistics">
	        <div class="card-body">
	          <div class="clearfix">
	            <div class="float-left">
	              <i class="mdi mdi-receipt text-warning icon-lg"></i>
	            </div>
	            <div class="float-right">
	              <p class="mb-0 text-right">Enroll Employee</p>
	              <div class="fluid-container">
	                <h3 class="font-weight-medium text-right mb-0">530</h3>
	              </div>
	            </div>
	          </div>
	          <p class="text-muted mt-3 mb-0">
	            {{-- <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> --}}Enroll Employee 
	          </p>
	        </div>
	      </div>
	    </div>
	    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
	      <div class="card card-statistics">
	        <div class="card-body">
	          <div class="clearfix">
	            <div class="float-left">
	              <i class="mdi mdi-poll-box text-success icon-lg"></i>
	            </div>
	            <div class="float-right">
	              <p class="mb-0 text-right">Absent Today</p>
	              <div class="fluid-container">
	                <h3 class="font-weight-medium text-right mb-0">30</h3>
	              </div>
	            </div>
	          </div>
	          <p class="text-muted mt-3 mb-0">
	            {{-- <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> --}}Employee That Absent For Today
	          </p>
	        </div>
	      </div>
	    </div>
	    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
	      <div class="card card-statistics">
	        <div class="card-body">
	          <div class="clearfix">
	            <div class="float-left">
	              <i class="mdi mdi-account-location text-info icon-lg"></i>
	            </div>
	            <div class="float-right">
	              <p class="mb-0 text-right">Active Employees</p>
	              <div class="fluid-container">
	                <h3 class="font-weight-medium text-right mb-0">500</h3>
	              </div>
	            </div>
	          </div>
	          <p class="text-muted mt-3 mb-0">
	          {{--   <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> --}}List Employee That Use Workstation
	          </p>
	        </div>
	      </div>
	    </div>
	  </div>
      <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">In Time Statisic</h4>
              <canvas id="lineChart" style="height:250px"></canvas>
            </div>
          </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Attendance Statistic</h4>
              <canvas id="barChart" style="height:230px"></canvas>
            </div>
          </div>
        </div>
      </div>
</div>
<!-- content-wrapper ends -->
@stop